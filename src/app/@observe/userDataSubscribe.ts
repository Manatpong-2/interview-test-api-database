import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";

@Injectable({
  providedIn: "root",
})
export class UserDataSubscribe {

  private user = new BehaviorSubject<any>(null);
  getUserData = this.user.asObservable();

  setUserData(data: object[]) {
    this.user.next(data);
  }

}
