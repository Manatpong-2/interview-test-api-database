import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataSubscribe } from 'src/app/@observe/userDataSubscribe';
import { AuthService } from 'src/app/@services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private _router: Router,
    private _authService: AuthService,
    private _userDataSubscribe: UserDataSubscribe
  ) { }

  userObj = JSON.parse(localStorage.getItem("userData"));
  timerInterval: any;
  imgUrl: string = null;

  ngOnInit(): void {
    this.loadPage();
    this.checkAuthInterval();
    this._userDataSubscribe.getUserData.subscribe(data => {
      if (data) {
        this.userObj = data;
      }
    });
  }

  ngOnDestroy() {
    clearInterval(this.timerInterval);
  }

  logout() {
    if (confirm('Do you want to logout?')) {
      clearInterval(this.timerInterval);
      this._authService.logout(this.userObj['token']).subscribe((data) => {
        if (data['status'] == 1) {
          localStorage.removeItem("userData");
          this._router.navigate(["/login"]);
        } else {
          alert(data['msg']);
        }
      })
    }
  }

  loadPage() {
    if (this.userObj) {
      this._authService.authCheck(this.userObj['token']).subscribe((data) => {
        if (data['status'] == 1) {
          if (this.userObj['img_path']) {
            const url_str = String(window.location.href).split("/");
            const root_url = String(url_str[2]).split(":");
            this.imgUrl = url_str[0] + '//' + root_url[0] + this.userObj['img_path'];
          }
          this._router.navigate(["/home", { outlets: { home: ["profile"] } }]);
        } else {
          alert('Your session timeout!')
          localStorage.removeItem("userData");
          this._router.navigate(["/login"]);
        }
      })
    } else {
      this._router.navigate(["/login"]);
    }
  }

  checkAuthInterval() {
    this.timerInterval = setInterval(() => {
      this.checkAuth();
    }, 10000)
  }

  checkAuth() {
    if (this.userObj) {
      this._authService.authCheck(this.userObj['token']).subscribe((data) => {
        if (data['status'] == -1) {
          alert('Your session timeout!')
          localStorage.removeItem("userData");
          this._router.navigate(["/login"]);
        }
      })
    } else {
      this._router.navigate(["/login"]);
    }
  }
}
