import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm, ValidationErrors } from '@angular/forms';
import { UserDataSubscribe } from 'src/app/@observe/userDataSubscribe';
import { UserService } from 'src/app/@services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profileForm: FormGroup;
  isSubmit: boolean = false;
  errorMsg: any = null;
  userObj = JSON.parse(localStorage.getItem("userData"));
  loading: boolean = false;
  imgUrl: string = null;
  submitErrorMsg: string = null;
  userData: object = null;
  editProfile: boolean = false;
  enableChangePassword: boolean = false;
  tmpImgName: string = null;
  constructor(
    private _userService: UserService,
    private formBuilder: FormBuilder,
    private _userDataSubscribe: UserDataSubscribe
  ) {
    this.profileForm = this.formBuilder.group({
      firstname: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
        Validators.pattern('^[A-Za-z]*$')]),
      lastname: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
        Validators.pattern('^[A-Za-z]*$')]),
      old_password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ]),
      confirm_password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ]),
      profile_img: null,
    });

  }

  ngOnInit(): void {
    this.profileForm.get('firstname').setValue(this.userObj['firstname']);
    this.profileForm.get('lastname').setValue(this.userObj['lastname']);
    this.initForm();
  }

  initForm() {
    this.isSubmit = false;
    this.editProfile = false;
    this.profileForm.controls['firstname'].disable();
    this.profileForm.controls['lastname'].disable();

    this.profileForm.get('old_password').setValue(null);
    this.profileForm.get('password').setValue(null);
    this.profileForm.get('confirm_password').setValue(null);
    if (this.userObj['img_path']) {
      const url_str = String(window.location.href).split("/");
      const root_url = String(url_str[2]).split(":");
      this.imgUrl = url_str[0] + '//' + root_url[0] + this.userObj['img_path'];
    }
    this.enableChangePassword = false
  }

  getUserData() {
    this._userService.getUser(this.userObj['user_id']).subscribe((data) => {
      const userData = data['data'];
      this.userObj['firstname'] = userData['firstname']
      this.userObj['lastname'] = userData['lastname']
      this.userObj['img_path'] = userData['img_path']
      localStorage.setItem("userData", JSON.stringify(this.userObj)); // update data in localstorage
      this._userDataSubscribe.setUserData(this.userObj);
    })
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      let file_extension = file.name.split('.').pop();
      if (file_extension != 'jpg') {
        alert('Invalid file extension')
      } else {
        this.profileForm.get('profile_img').setValue(file);
        const formData = new FormData();
        formData.append('profile_img', this.profileForm.get('profile_img').value);
        this._userService.uploadImg(formData).subscribe((data) => {
          if (data['status'] == 1) {
            if (data['data']['filename']) {
              const url_str = String(window.location.href).split("/");
              const root_url = String(url_str[2]).split(":");
              this.imgUrl = url_str[0] + '//' + root_url[0] + '/images/' + data['data']['filename'];
              this.tmpImgName = data['data']['filename'];
            }
          } else {
            alert(data['msg']);
          }
        })
      }
    }
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  validateData() {
    if (this.enableChangePassword) {
      if ((this.profileForm.valid && (this.profileForm.get('password').value == this.profileForm.get('confirm_password').value))) {
        return true;
      } else {
        return false;
      }
    } else {
      if (this.profileForm.get('firstname').errors || this.profileForm.get('lastname').errors) {
        return false;
      } else {
        return true;
      }
    }
  }

  updateProfile() {
    this.isSubmit = true;
    if (this.validateData()) {
      if (confirm('Do you want to save the information?')) {
        this.errorMsg = null;
        this.loading = true;
        const formData = new FormData();
        formData.append('firstname', this.profileForm.get('firstname').value);
        formData.append('lastname', this.profileForm.get('lastname').value);
        if (this.enableChangePassword) {
          formData.append('old_password', this.profileForm.get('old_password').value);
          formData.append('new_password', this.profileForm.get('password').value);
        }
        if (this.profileForm.get('profile_img').value) {
          formData.append('profile_img', this.profileForm.get('profile_img').value);
        }

        if (this.tmpImgName) {
          formData.append('tmp_img_name', this.tmpImgName);
        }


        this._userService.updateProfile(this.userObj['user_id'], formData).subscribe(async (data) => {
          if (data['status'] == 1) {
            await this.getUserData();
            await this.delay(1)
            this.profileForm.get('firstname').setValue(data['data']['firstname']);
            this.profileForm.get('lastname').setValue(data['data']['lastname']);
            this.initForm()
            if (this.profileForm.get('profile_img').value) {
              await this.delay(500)
              this.refreshPage() // to refresh image
            }
          }
          this.loading = false;
          alert(data['msg']);
        })
      }
    } else {
      this.errorMsg = this.getFormValidationErrorMsg();
    }
  }

  isError(param: string) {
    if (this.isSubmit && this.profileForm.get(param)?.errors) {
      return 'is-invalid';
    } else {
      return '';
    }
  }

  refreshPage() {
    window.location.reload();
  }

  getParamErrorMsg(param) {
    const errors = this.profileForm.get(param)?.errors;
    if (errors) {
      if ('minlength' in errors) {
        return 'The ' + param + ' must be at least ' + errors['minlength']['requiredLength'] + ' characters';
      } else if ('required' in errors) {
        return 'The ' + param + ' is required';
      } else if ('pattern' in errors) {
        return 'The ' + param + ' is invalid format';
      } else {
        return 'Unknown error';
      }
    }
    return '';

  }

  getFormValidationErrorMsg() {
    let errMsg = null;
    Object.keys(this.profileForm.controls).some(key => {
      const errors = this.profileForm.get(key)?.errors;
      if (errors) {
        if ('minlength' in errors) {
          errMsg = 'The ' + key + ' must be at least ' + errors['minlength']['requiredLength'] + ' characters';
        } else if ('required' in errors) {
          errMsg = 'The ' + key + ' is required';
        } else if ('pattern' in errors) {
          errMsg = 'The ' + key + ' is invalid format';
        } else {
          errMsg = 'Unknown error';
        }
        return true;
      }
    });
    return errMsg;

  }

  enableEditProfile() {
    this.editProfile = true;
    this.profileForm.controls['firstname'].enable();
    this.profileForm.controls['lastname'].enable();
  }

  cancelEditProfile() {
    this.editProfile = false;

    this.profileForm.controls['firstname'].disable();
    this.profileForm.controls['lastname'].disable();
    this.enableChangePassword = false;
  }

  changePassword(event) {
    this.enableChangePassword = event.srcElement.checked;
  }
}
